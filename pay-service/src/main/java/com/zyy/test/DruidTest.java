package com.zyy.test;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * DruidTest
 *
 * @author zhougf
 * @date 2019/8/13
 */
public class DruidTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
        DruidDataSource druidDataSource = (DruidDataSource) context.getBean("dataSource");
        System.out.println(druidDataSource);
    }
}
