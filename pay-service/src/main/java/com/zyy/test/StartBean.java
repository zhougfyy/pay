package com.zyy.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * StartBean
 *
 * @author zhougf
 * @date 2019/7/30
 */
public class StartBean {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
    }
}
