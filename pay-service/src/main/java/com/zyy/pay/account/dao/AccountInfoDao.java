package com.zyy.pay.account.dao;

import com.zyy.pay.account.bean.AccountInfoBean;

/**
 * AccountInfoDao
 *
 * @author zhougf
 * @date 2019/7/30
 */
public interface AccountInfoDao {
    /**
     * saveAccountInfo 保存accountInfo对象
     *
     * @param accountInfoBean accountInfo对象
     * @return Integer 1 :保存成功
     * */
    Integer saveAccountInfo(AccountInfoBean accountInfoBean);
}
