package com.zyy.pay.account.service;

import com.zyy.pay.account.bean.AccountInfoBean;

/**
 * AccountInfoService
 *
 * @author zhougf
 * @date 2019/7/30
 */
public interface AccountInfoService {

    /**
     * saveAccountInfo 保存accountInfo对象
     *
     * @param accountInfoBean accountInfo对象
     * @return Integer 1 :保存成功
     * */
    Integer saveAccountInfo(AccountInfoBean accountInfoBean);
}
