package com.zyy.pay.account.bean;

/**
 * AccountInfoBean
 *
 * @author zhougf
 * @date 2019/7/29
 */
public class AccountInfoBean {

    private Integer accountId;

    private String accountNo;

    private String accountType;

    private Long accountUserId;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Long getAccountUserId() {
        return accountUserId;
    }

    public void setAccountUserId(Long accountUserId) {
        this.accountUserId = accountUserId;
    }
}
