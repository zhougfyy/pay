package com.zyy.pay.account.service.impl;

import com.zyy.pay.account.bean.AccountInfoBean;
import com.zyy.pay.account.dao.AccountInfoDao;
import com.zyy.pay.account.service.AccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * AccountInfoServiceImpl
 *
 * @author zhougf
 * @date 2019/7/30
 */
@Service
public class AccountInfoServiceImpl implements AccountInfoService {

    @Autowired
    private AccountInfoDao accountInfoDao;

    /*public AccountInfoServiceImpl() {}

    @Autowired
    public AccountInfoServiceImpl(AccountInfoDao accountInfoDao) {
        this.accountInfoDao = accountInfoDao;
    }*/

    public Integer saveAccountInfo(AccountInfoBean accountInfoBean) {
        return accountInfoDao.saveAccountInfo(accountInfoBean);
    }
}
