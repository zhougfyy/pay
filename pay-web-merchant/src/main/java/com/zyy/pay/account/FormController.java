package com.zyy.pay.account;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * FormController
 *
 * @author zhougf
 * @date 2019/8/16
 */
@Controller
public class FormController {

    @RequestMapping("/{formName}")
    public String formController(@PathVariable String formName) {
        return formName;
    }
}
