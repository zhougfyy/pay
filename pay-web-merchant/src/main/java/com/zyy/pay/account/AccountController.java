package com.zyy.pay.account;

import com.zyy.pay.account.bean.AccountInfoBean;
import com.zyy.pay.account.service.AccountInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * AccountController
 *
 * @author zhougf
 * @date 2019/8/16
 */
@Controller
public class AccountController {

    @Autowired
    private AccountInfoService accountInfoService;

    @RequestMapping(value = "/saveAccount", method = RequestMethod.POST)
    public String saveAccount(AccountInfoBean accountInfoBean) {
        accountInfoService.saveAccountInfo(accountInfoBean);
        System.out.println(accountInfoBean);
        return "home";
    }

}
