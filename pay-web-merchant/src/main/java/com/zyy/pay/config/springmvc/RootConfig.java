package com.zyy.pay.config.springmvc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * RootConfig
 *
 * @author zhougf
 * @date 2019/8/16
 */
@Configuration
@ComponentScan(value = {"com.zyy.pay"}, excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION,
        value = EnableWebMvc.class)})
public class RootConfig {
}
